#!/usr/bin/env python

"""
Main training script for DAS machine learning
"""

__copyright__ = """
Machine Learning for Distributed Acoustic Sensing data (MLDAS)
Copyright (c) 2020, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of
any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative 
works, and perform publicly and display publicly, and to permit others to do so.
"""
__license__ = "Modified BSD license (see LICENSE.txt)"
__maintainer__ = "Vincent Dumont"
__email__ = "vincentdumont11@gmail.com"

# System
import os
import logging

# Externals
import yaml
import numpy
import torch.distributed as dist

# Locals
from .datasets import get_data_loaders
from .trainers import get_trainer
from .utils.logging import config_logging
from .utils.distributed import init_workers

def load_config(config_file,**kwargs):
    """
    """
    with open(config_file) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    config.update({k:v for (k,v) in kwargs.items() if v})
    update = {k:v for (k,v) in kwargs.items() if v}
    config['output_dir'] = os.path.expandvars(config['output_dir'])
    os.makedirs(config['output_dir'], exist_ok=True)
    return config

def train(config,distributed_backend,verbose=False,rank_gpu=False,ranks_per_node=8,gpu=None,**kwargs):
    """Main function"""
    # Initialization
    rank, n_ranks, device, device_ids = init_workers(distributed_backend)
    # Load configuration
    config = load_config(config,**kwargs)
    # Setup logging
    log_file = os.path.join(config['output_dir'], 'out_%i.log' % rank)
    config_logging(verbose=verbose, log_file=log_file)
    logging.info('Initialized rank %i out of %i', rank, n_ranks)
    if rank == 0:
        logging.info('Configuration: %s' % config)
    # Load the datasets
    is_distributed = distributed_backend is not None
    train_data_loader, valid_data_loader, test_data_loader = get_data_loaders(is_distributed, **config)
    target_data, label_data = next(iter(train_data_loader))
    input_size = target_data.reshape(target_data.shape[0],-1).shape[1]
    output_size = label_data.reshape(label_data.shape[0],-1).shape[1]
    if 'layers' in config.keys():
        config['layers'] = [input_size]+config['layers']+[output_size]
    trainer = get_trainer(rank=rank, device=device, **config)
    # Build the model
    trainer.build_model(device_ids,distributed=is_distributed,**config)
    if rank == 0:
        trainer.print_model_summary()
    # Run the training
    summary = trainer.train(train_data_loader=train_data_loader, valid_data_loader=valid_data_loader, test_data_loader=test_data_loader, **config)
    trainer.write_summaries()
    # Print some conclusions
    logging.info('Finished training')
    logging.info('Train samples %g time %g s rate %g samples/s',
                 numpy.mean(summary['train_samples']),
                 numpy.mean(summary['train_time']),
                 numpy.mean(summary['train_rate']))
    if valid_data_loader is not None:
        logging.info('Valid samples %g time %g s rate %g samples/s',
                     numpy.mean(summary['valid_samples']),
                     numpy.mean(summary['valid_time']),
                     numpy.mean(summary['valid_rate']))
    logging.info('All done!')
    return summary['test_loss']
    